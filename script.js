var NAME_MAIN_TABLE = 'Sheet1';
var NAME_OTHER_TABLE = 'Список задач';

function onOpen() {
    SpreadsheetApp.getUi()
        .createMenu('Парсер таблиц')
        .addItem('Запустить', 'showDialog')
        .addToUi();
}

function showDialog() {
    var html = HtmlService.createHtmlOutputFromFile('index')
        .setWidth(750)
        .setHeight(740);
    SpreadsheetApp.getUi()
        .showModalDialog(html, 'Парсер таблиц в одну таблицу');
}


// Получает ссылку на таблицу и возвращает имя, ссылку, айди таблицы
function getSpreadsheetInfo(url) {
    var id = getSheetId(url);
    var client = SpreadsheetApp.openById(id).getName();

    return {
        client: client,
        id: id,
        url: url
    };
}

function updateData(sheet) {
    var main_sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName(NAME_MAIN_TABLE);

    // Получаем данные из таблицы по айди
    var sheet_data = getDataFromSheet(sheet.id, NAME_OTHER_TABLE);

    // Стартовый ряд диапазона
    var row = sheet.rowNum[0] + 1;

    // Стартовая колонка диапазона
    var column = 2;

    // Кол-во добавляемых строк и колонок
    var num_rows = sheet_data.length;
    var num_columns = sheet_data[0].length;

    // Добавить ячейки
    if (num_rows > sheet.rowNum.length) {
        var count = num_rows - sheet.rowNum.length;
        main_sheet.insertRowsAfter(row, count);
    }

    // Удалить лишние ячейки
    if (num_rows < sheet.rowNum.length) {
        var count = sheet.rowNum.length - num_rows;
        main_sheet.deleteRows(row, count);
    }

    // Присоединяем данные из другой таблицы
    main_sheet.getRange(row, column, num_rows, num_columns).setValues(sheet_data);

    // Добавляем название документа в колонку «Клиент»
    var sheet_name = sheet.client;
    var link = sheet.url;
    main_sheet.getRange(row, main_sheet.getLastColumn(), sheet_data.length).setFormula('=HYPERLINK("' + link + '", "' + sheet_name + '")');

    addIDRows();
}

function deleteDataFromSheet(sheet) {
    var main_sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName(NAME_MAIN_TABLE);
    var row = sheet.rowNum[0] + 1;
    var count = sheet.rowNum.length;
    main_sheet.deleteRows(row, count);
    addIDRows();
}

function getDataFromSheet(id, name) {
    var other_sheet = SpreadsheetApp.openById(id).getSheetByName(name);

    // Диапазон с которого берем данные
    // Начинаем с A2, т.к. А1 это шапка
    var sheet_range = other_sheet.getRange('A2:Z9999');

    // Забираем данные с таблицы
    var sheet_data = sheet_range.getValues();

    // Убираем строки где нет данных
    return sheet_data.filter(function (array) {
        return !array.every(function (item) {
            return item === '' || item.length === 0;
        });
    });
}

function cloneGoogleSheet(sheet_id) {
    var main_sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName(NAME_MAIN_TABLE);

    sheet_id.forEach(function (id, index) {
        var sheet_data = getDataFromSheet(id, NAME_OTHER_TABLE);

        var last_row = main_sheet.getLastRow();

        // Шапка сверху таблицы
        var header = 1;

        // Стартовый ряд диапазона
        var row = main_sheet.getLastRow() + header;

        // Стартовая колонка диапазона
        var column = 2;

        // Кол-во добавляемых строк и колонок
        var num_rows = sheet_data.length;
        var num_columns = sheet_data[0].length;

        // Присоединяем данные из другой таблицы
        main_sheet.getRange(row, column, num_rows, num_columns).setValues(sheet_data);

        // Добавляем название документа в колонку «Клиент»
        var sheet_name = SpreadsheetApp.openById(id).getName();
        var link = 'https://docs.google.com/spreadsheets/d/' + sheet_id[index];
        main_sheet.getRange(last_row + header, main_sheet.getLastColumn(), sheet_data.length).setFormula('=HYPERLINK("' + link + '", "' + sheet_name + '")');
    });

    // Добавляем порядковый номер для каждой ячейки
    addIDRows();
}

function addIDRows() {
    var COLUMN = 0;
    var HEADER_ROW_COUNT = 1;

    var spreadsheet = SpreadsheetApp.getActiveSpreadsheet();
    var mainsheet   = spreadsheet.getSheetByName(NAME_MAIN_TABLE);
    var rows        = mainsheet.getDataRange().getNumRows();
    var vals        = mainsheet.getSheetValues(1, 1, rows, 2);

    for (var row = HEADER_ROW_COUNT; row < vals.length; row++) {
        try {
            var id = vals[row][COLUMN];
            mainsheet.getRange(row + 1, COLUMN + 1).setValue(row);
        } catch(ex) {
            console.log('Error: ' + ex)
        }
    }
}

function findOtherTable() {
    var spreadsheet = SpreadsheetApp.getActiveSpreadsheet();
    var mainsheet   = spreadsheet.getSheetByName(NAME_MAIN_TABLE);

    // Получим все данные из колонки Клиент
    var sheet_range = mainsheet.getRange('U2:U9999');
    var sheet_data = sheet_range.getValues();

    sheet_data = sheet_data.filter(function (array) {
        return !array.every(function (item) {
            return item === '' || item.length === 0;
        });
    });

    // Получать список клиентов из столбца
    sheet_data = searchUnique(sheet_data);

    return sheet_data;
}

function searchUnique(arr) {
    var obj = {};

    for (var i = 0; i < arr.length; i++) {
        var str = arr[i];
        obj[str] = true;
    }

    return Object.keys(obj);
}

// Получить все строки из главной таблицы
// Вернуть объект который содержит все таблицы и их ссылки
// {
// client: "[A2A][65apps] Список задач",
// rowNum: [1, 2, 3, 4, 5, 6... 28],
// url: https://docs.google.com/spreadsheets/d/1J9MAtfKoS3a08MegCxZyRTZCqKAdjM_fedFthqWCdq8,
// id: '1J9MAtfKoS3a08MegCxZyRTZCqKAdjM_fedFthqWCdq8'
// }
//TODO написать свою реализацию, чтобы не обязательно колонка была с названием по английски
function searchDataInMainTable() {
    var range = SpreadsheetApp.getActiveSheet().getDataRange().getValues();

    // Найти ссылки на таблицы
    var urls = searchUrlsInMainTable();

    // Получить кординаты для строк для таблиц
    var array = ObjApp.rangeToObjects(range);

    // Добавить массив координат ячеек в объект ссылок
    urls.forEach(function (obj) {
        var tables_data = array.filter(function (item) {
            return item.client === obj.client;
        });

        obj.rowNum = tables_data.map(function (item) {
            return item['rowNum'];
        });
    });

    return urls;
}

// Найти все формулы с ссылками на оригиналы таблиц
// Вернуть массив объектов → client - имя таблицы, url - ссылка на таблицу, id - id таблицы
function searchUrlsInMainTable() {
    var spreadsheet = SpreadsheetApp.getActiveSpreadsheet();
    var mainsheet   = spreadsheet.getSheetByName(NAME_MAIN_TABLE);
    var range = mainsheet.getDataRange();

    var formulas = range.getFormulas();

    var data = [];

    // Получить все формулы, где есть HYPERLINK
    formulas.forEach(function (array) {
        if (array.length) {
            var hyperlink = array.filter(function (cell) {
                return cell.search('HYPERLINK') !== -1
            });

            if (hyperlink.length) {
                data.push(hyperlink);
            }
        }
    });

    // Убрать все повторяющиеся значения
    var url = searchUnique(data);

    return url.map(function (hyperlink) {
        return {
            // Убираем скобки, кавычки и пробелы
            client: hyperlink.split(',')[1].replace(')', '').replace(/"/g, '').trim(),
            url: getSheetUrl(hyperlink.split(',')[0]).toString(),
            id: getSheetId(hyperlink.split(',')[0]).toString()
        };
    });
}

function getSheetId(url) {
    var id = url.match(/\/d\/[a-zA-Z0-9-_]*/g);
    return id.toString().replace('/d/', '');
}

function getSheetUrl(url) {
    return url.match(/https:\/\/docs.google.com\/spreadsheets\/d\/[a-zA-Z0-9-_]*/g);
}